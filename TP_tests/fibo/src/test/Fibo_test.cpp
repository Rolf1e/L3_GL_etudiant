//
// Created by rolfie on 4/27/20.
//
#include <string>

#include <CppUTest/CommandLineTestRunner.h>
#include "../main/Fibo.hpp"

using namespace std;

TEST_GROUP(GroupFibo) {
};

TEST(GroupFibo, checkNumber) {

    CHECK_EQUAL(0, fibo(0));
    CHECK_EQUAL(1, fibo(1));
    CHECK_EQUAL(1, fibo(2));
    CHECK_EQUAL(2, fibo(3));
    CHECK_EQUAL(3, fibo(4));
}

TEST(GroupFibo, exception) {
    CHECK_THROWS(string, fibo(50));
    CHECK_THROWS(string, fibo(-1));
}
