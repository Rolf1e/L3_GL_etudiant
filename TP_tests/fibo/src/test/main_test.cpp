//
// Created by rolfie on 4/27/20.
//

#include <CppUTest/CommandLineTestRunner.h>

int main(int argc, char ** argv)
{
    return CommandLineTestRunner::RunAllTests(argc, argv);
}
