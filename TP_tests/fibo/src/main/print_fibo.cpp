#include "Fibo.hpp"
#include <iostream>

using namespace std;

int main() {
    for (int i = 0; i < 50; i++) {
        try {
            int number = fibo(i - 1);
            std::cout << number << std::endl;
        } catch (string e) {
            cout << e << endl;
        }
//        assert(number >= 0);
//        assert(fibo(i) >= number);
    }
    return 0;
}

