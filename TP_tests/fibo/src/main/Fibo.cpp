#include <assert.h>
#include <string>
#include "Fibo.hpp"

using namespace std;

int fibo(int n, int f0, int f1) {
    if(n < 0) {
        throw string("n < 0");
    }
    if (f0 < 0 && f1 <= 0) {
        throw string("f0 < 0 || f1 < 0");
    }
    if (f0 > f1) {
        throw string("f0 > f1");
    }
//    assert(f0 >= 0 && f1 > 0);
//    assert(f0 <= f1);
    return n <= 0 ? f0 : fibo(n - 1, f1, f1 + f0);
}

