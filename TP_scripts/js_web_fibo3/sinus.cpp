//
// Created by rolfie on 3/27/20.
//

#include <cmath>

double sinus(double x, double a, double b) {
    return sin(2 * M_PI * (a * x + b));
}

#include <emscripten/bind.h>

EMSCRIPTEN_BINDINGS(sinus) {
        emscripten::function("sinus", &sinus);
}

